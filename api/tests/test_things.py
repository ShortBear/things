from main import app
from fastapi.testclient import TestClient
from queries.things import ThingsQueries
from models import Thing, ThingParams
from authenticator import authenticator

client = TestClient(app)


def fake_get_current_account_data():
    return {
        'id': 'fakeuser'
    }


class ThingQueriesMock:
    def create(self, params: ThingParams, user_id: str) -> Thing:
        thing = params.dict()
        thing['id'] = '1337'
        thing['user_id'] = user_id
        return Thing(**thing)

    def get_all(self, user_id: str) -> list[Thing]:
        return [Thing(id='1337', name='Thing 1', user_id=user_id)]

    def delete(self, _id: str) -> bool:
        return True


def test_create_thing():
    overrides = {
        ThingsQueries: ThingQueriesMock,
        authenticator.get_current_account_data: fake_get_current_account_data
    }
    app.dependency_overrides = overrides
    thing = {
        'name': 'Thing 1'
    }
    res = client.post('/api/things', json=thing)
    data = res.json()
    assert data['id'] == '1337'
    assert data['user_id'] == 'fakeuser'

    app.dependency_overrides = {}


def test_get_all_things():
    overrides = {
        ThingsQueries: ThingQueriesMock,
        authenticator.get_current_account_data: fake_get_current_account_data
    }
    app.dependency_overrides = overrides
    res = client.get('/api/things')
    data = res.json()
    assert len(data['things']) == 1
    assert data['things'][0]['user_id'] == 'fakeuser'
    app.dependency_overrides = {}
