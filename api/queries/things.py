from queries.client import Queries
from models import Thing, ThingParams
from bson.objectid import ObjectId


class ThingsQueries(Queries):
    COLLECTION = 'things'

    def create(self, params: ThingParams, user_id: str) -> Thing:
        thing = params.dict()
        thing['user_id'] = user_id
        self.collection.insert_one(thing)
        thing['id'] = str(thing['_id'])
        return Thing(**thing)

    def get_all(self, user_id: str) -> list[Thing]:
        things = []
        for thing in self.collection.find({"user_id": user_id}):
            thing['id'] = str(thing['_id'])
            things.append(Thing(**thing))
        return things

    def delete(self, id: str, user_id: str) -> bool:
        result = self.collection.delete_one(
            {'_id': ObjectId(id), 'user_id': user_id}
        )
        return result.deleted_count == 1
