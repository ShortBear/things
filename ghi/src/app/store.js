import { configureStore } from '@reduxjs/toolkit'
import newThingReducer from '../features/thing/newThingSlice'
import loginReducer from '../features/auth/loginSlice'
import signupReducer from '../features/auth/signupSlice'
import { setupListeners } from '@reduxjs/toolkit/query'
import { thingsApi } from '../services/things'

export const store = configureStore({
  reducer: {
    newThing: newThingReducer,
    login: loginReducer,
    signup: signupReducer,
    [thingsApi.reducerPath]: thingsApi.reducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat([thingsApi.middleware])
})

setupListeners(store.dispatch)
